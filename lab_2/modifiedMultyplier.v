module ModifiedMultiplier(
  input wire clk,
  input wire reset,
  input wire [N-1:0] operand1,
  input wire [N-1:0] operand2,
  output reg [2*N-1:0] result
);

  reg [2*N-1:0] next_result;

  always @(posedge clk or posedge reset) begin
    if (reset) begin
      result <= {2*N{1'b0}};
    end else begin
      result <= next_result;
    end
  end

  always @(operand1, operand2) begin
    next_result = operand1 * operand2;
  end

endmodule