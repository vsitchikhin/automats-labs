module DirectToModified(
  input wire clk,
  input wire reset,
  input wire [N-1:0] direct,
  output reg [N-1:0] modified
);

  reg [N-1:0] next_modified;

  always @(posedge clk or posedge reset) begin
    if (reset) begin
      modified <= {N{1'b0}};
    end else begin
      modified <= next_modified;
    end
  end

  always @(direct) begin
    next_modified = direct ^ (direct >> 1);
  end

endmodule