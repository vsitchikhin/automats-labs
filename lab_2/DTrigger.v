module MooreAutomaton(
  input wire I,
  input wire CLK,
  output reg O
);

  reg [1:0] state;
  reg D1, D0;

  always @(posedge CLK) begin
    case(state)
      2'b00: begin
        D1 <= (state[1] & ~I) | (state[0] & I);
        D0 <= (state[1] & ~I) | (state[0] & ~I);
        state <= D1;
      end
      2'b01: begin
        D1 <= (state[1] & I) | (state[0] & I);
        D0 <= (state[1] & ~I) | (state[0] & I);
        state <= D1;
      end
      2'b10: begin
        D1 <= (state[1] & I) | (state[0] & ~I);
        D0 <= (state[1] & ~I) | (state[0] & I);
        state <= D1;
      end
      2'b11: begin
        D1 <= (state[1] & ~I) | (state[0] & I);
        D0 <= (state[1] & I) | (state[0] & ~I);
        state <= D1;
      end
    endcase
  end

  always @(posedge CLK) begin
    O <= state[1];
  end

endmodule