import * as fs from "fs";

console.log("Поиск дат в формате dd.mm.yy и dd.mm.yyyy");
console.log('Данное РВ выполняет поиск всех дат указанного формата в тексте');
console.log('\n----------------------------------\n');

const text = fs.readFileSync('./text.txt', 'utf-8')

const pattern = /\b\d{2}\.\d{2}\.\d{2}(?:\d{2})?\b/g;

const matches = text.match(pattern);


console.info('\nИсходный текст - ', text)
console.log('\n----------------------------------\n');
console.info('Найденные значения - ', matches);
console.log('\n----------------------------------\n');
console.info('Количество найденных значений - ', matches?.length);
console.log('\n----------------------------------\n');

