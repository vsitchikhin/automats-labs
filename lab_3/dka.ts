function dfaRecognizer(inputString: string) {
  let currentState = 'A'; // Начальное состояние

  for (let i = 0; i < inputString.length; i++) {
    const char = inputString[i];

    if (currentState === 'A') {
      if (/\d/.test(char)) {
        currentState = 'B';
      } else {
        return false;
      }
    } else if (currentState === 'B') {
      if (/\d/.test(char)) {
        currentState = 'C';
      } else if (char === '.') {
        currentState = 'D';
      } else {
        return false;
      }
    } else if (currentState === 'C') {
      if (char === '.') {
        currentState = 'D';
      } else {
        return false;
      }
    } else if (currentState === 'D') {
      if (/\d/.test(char)) {
        currentState = 'E';
      } else {
        return false;
      }
    } else if (currentState === 'E') {
      if (char === '.') {
        currentState = 'F';
      } else if (/\d/.test(char)) {
        currentState = 'E';
      } else {
        return false;
      }
    } else if (currentState === 'F') {
      if (/\d/.test(char)) {
        currentState = 'G';
      } else {
        return false;
      }
    } else if (currentState === 'G') {
      if (/\d/.test(char)) {
        currentState = 'G';
      } else {
        return false;
      }
    }
  }

  return currentState === 'G'; // Вернуть true, если достигнуто конечное состояние
}

// Пример использования
const inputString = prompt("Введите строку:") || '';
if (dfaRecognizer(inputString)) {
  console.log("Строка соответствует регулярному выражению");
} else {
  console.log("Строка не соответствует регулярному выражению");
}